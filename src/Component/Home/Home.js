import React, { useEffect, useState } from "react";
import axios from "axios";
import GamesData from "../GamesData/GamesData";
import Navigation from "../UI/Navigation";
import MediaCard from "../UI/Card";
import ListData from "../UI/listData";
// import SortByPlatform from "../UI/SortByPlatform";

const Home = () => {
  const [apiData, setApiData] = useState(null);
  const [searchData, setSearchData] = useState("");
  const [searchCheck, setSearchCheck] = useState(false);

  const baseURL =
    "https://s3-ap-southeast-1.amazonaws.com/he-public-data/gamesarena274f2bf.json";

  useEffect(() => {
    async function getGameData() {
      const response = await axios.get(baseURL).then((response) => {
        return response;
      });
      let mb = response.data.shift();
      setApiData(response.data);
    }
    getGameData();
  }, []);

  return (
    <>
      {apiData && <Navigation gameData={apiData} />}
      {/* {apiData && <GamesData gameData={apiData}  />} */}
    </>
  );
};

export default Home;
