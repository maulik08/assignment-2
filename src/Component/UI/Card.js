import * as React from "react";
import Card from "@mui/material/Card";
import CardContent from "@mui/material/CardContent";
import Grid from "@mui/material/Grid";

export default function MediaCard(props) {
  return (
    <Grid
      container
      spacing={0}
      direction="column"
      alignItems="center"
      justify="center"
      style={{ minHeight: "100vh", margin: "auto" }}
    >
      <Grid item xs={3}>
        <Card sx={{ maxWidth: 1000 }}>
          <CardContent>{props.children}</CardContent>
        </Card>
      </Grid>
    </Grid>
  );
}
