import React, { useState } from "react";
import List from "@mui/material/List";
import ListItem from "@mui/material/ListItem";
import Divider from "@mui/material/Divider";
import ListItemText from "@mui/material/ListItemText";
import ListItemAvatar from "@mui/material/ListItemAvatar";
import Avatar from "@mui/material/Avatar";
import Typography from "@mui/material/Typography";
import Stack from "@mui/material/Stack";
import Button from "@mui/material/Button";

const ListData = (props) => {
  const [yesSortData, setYesSortData] = useState(false);

  const sortData = () => {
    props.listData.sort((a, b) => {
      let aData = a.platform.toUpperCase();
      let bData = b.platform.toUpperCase();
      return aData > bData ? 1 : -1;
    });
  };

  const mapListData = props.listData.map((e) => {
    return (
      <>
        <ListItem alignItems="flex-start">
          <ListItemAvatar>
            <Avatar alt="Remy Sharp" src="/static/images/avatar/1.jpg" />
          </ListItemAvatar>
          <ListItemText
            primary={e.title}
            secondary={
              <React.Fragment>
                <Typography
                  sx={{ display: "block" }}
                  component="span"
                  variant="body2"
                  color="primary"
                >
                  Platform- {e.platform}
                </Typography>
                <Typography
                  sx={{ display: "block" }}
                  component="span"
                  variant="body2"
                  color="green"
                >
                  Score- {e.score}
                </Typography>
                <Typography
                  sx={{ display: "block" }}
                  component="span"
                  variant="body2"
                  color="blue"
                >
                  Genre- {e.genre}
                </Typography>
                <Typography
                  sx={{ display: "block" }}
                  component="span"
                  variant="body2"
                  color="text.primary"
                >
                  Editor's Choice- {e.editors_choice}
                </Typography>
              </React.Fragment>
            }
          />
        </ListItem>
        <Divider variant="inset" component="li" />
      </>
    );
  });

  const sortDataHandler = () => {
    setYesSortData(true);
    sortData();
  };

  return (
    <>
      <Stack spacing={2} direction="row">
        <Button variant="contained" onClick={sortDataHandler}>
          Sort Data
        </Button>
      </Stack>
      <List sx={{ width: "100%", maxWidth: 360, bgcolor: "background.paper" }}>
        {mapListData}
      </List>
    </>
  );
};

export default ListData;
