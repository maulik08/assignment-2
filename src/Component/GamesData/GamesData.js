import React from "react";
import MediaCard from "../UI/Card";
import ListData from "../UI/listData";

const GamesData = (props) => {
  return (
    <>
      <MediaCard>
        <ListData listData={props.gameData} />
      </MediaCard>
    </>
  );
};

export default GamesData;
